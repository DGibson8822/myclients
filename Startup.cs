﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyClients.Startup))]
namespace MyClients
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
